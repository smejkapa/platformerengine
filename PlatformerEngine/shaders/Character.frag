#version 330

in vec3 fragmentColor;
in vec2 UV;

out vec3 color;

void main() {
	color = fragmentColor;
}
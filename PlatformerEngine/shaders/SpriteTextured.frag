#version 330 core

in vec3 fragmentColor;
in vec2 fragmentUV;

uniform sampler2D diffuseTexture;

out vec4 color;

void main()
{
    vec4 texColor = texture(diffuseTexture, fragmentUV);
    if(texColor.a < 0.01)
        discard;
    color = texColor;
}
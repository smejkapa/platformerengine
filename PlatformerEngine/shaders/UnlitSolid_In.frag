#version 330 core

in vec3 fragmentColor;
in vec2 UV;

out vec4 color;

void main() {
	color = vec4(fragmentColor, 0.5);
}
#version 330 core

layout(location = 0) in vec3 vertexPosition_modelspace;
layout(location = 1) in vec3 vertexNormal;
layout(location = 2) in vec2 vertexUV;
layout(location = 3) in mat4 instanceMatrix;

uniform mat4 MVP;

out vec3 fragmentColor;
out vec2 UV;

void main() {
	gl_Position = MVP * instanceMatrix * vec4(vertexPosition_modelspace, 1);
	//gl_Position = MVP * vec4(vertexPosition_modelspace, 1);

	fragmentColor = vec3(0, 0, 0);
	UV = vertexUV;
}
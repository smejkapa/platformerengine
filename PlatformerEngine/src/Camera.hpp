﻿#pragma once

#include "gl/glew.h" // Glew first

#include "glm/glm.hpp"

#include "Entity.hpp"

namespace pe {

class Camera : public Entity {
protected:
    glm::vec3 upVector_;
    glm::vec3 frontVector_;

    float near_;
    float far_;

    Camera()
            : near_(0.0f)
            , far_(0.0f) {
    }

public:
    virtual ~Camera() = default;
    virtual glm::mat4 getViewMatrix() const = 0;

    glm::vec3 getFrontVector() const { return frontVector_; }
    glm::vec3 getUpVector() const { return upVector_; }
    float getNear() const { return near_; }
    float getFar() const { return far_; }

    void setFrontVector(glm::vec3 frontVector) { frontVector_ = frontVector; }
    void setUpVector(glm::vec3 upVector) { upVector_ = upVector; }
    void setNear(float near) { near_ = near; }
    void setFar(float far) { far_ = far; }
};

}

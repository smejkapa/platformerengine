﻿#pragma once
#include "Singleton.hpp"
#include "Camera.hpp"

namespace pe {

class CameraManager : public pe::Singleton<CameraManager> {
    friend Singleton<CameraManager>;

    Camera* mainCamera_;

    CameraManager() : mainCamera_(nullptr) {
    }

public:
    Camera* getMainCamera() const { return mainCamera_; }
    void setMainCamera(Camera* camera) { mainCamera_ = camera; }
};

}

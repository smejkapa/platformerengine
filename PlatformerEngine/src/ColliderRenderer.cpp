#include "ColliderRenderer.hpp"
#include "Box2D/Box2D.h"
#include "Draw.hpp"
#include "glm/gtc/matrix_transform.hpp"

namespace pe {

ColliderRenderer::ColliderRenderer(Entity* entity, b2Body* rigidBody)
        : entity_(entity)
        , rigidBody_(rigidBody)
        , shader_(ResourceManager::inst().getShader(Shader_r::id_t::ColliderDebugDraw)) {
    Draw::inst().subscribe(this);
    glGenVertexArrays(1, &VAO_);
    glGenBuffers(1, &VBO_);

    // TODO update vertices when the rigidBody_ is changed e.g. its vertices are moved around etc.
    // something that is not encapsulated in the rigidBody_.transform
    b2Fixture* fixture = rigidBody_->GetFixtureList();
    const b2Shape* shape = fixture->GetShape();
    switch (shape->GetType()) {
        case b2Shape::e_circle:
            vertices_ = generateVertices(dynamic_cast<const b2CircleShape*>(shape));
            break;
        case b2Shape::e_edge:
            vertices_ = generateVertices(dynamic_cast<const b2EdgeShape*>(shape));
            break;
        case b2Shape::e_polygon:
            vertices_ = generateVertices(dynamic_cast<const b2PolygonShape*>(shape));
            break;
        case b2Shape::e_chain:
            vertices_ = generateVertices(dynamic_cast<const b2ChainShape*>(shape));
            break;
        default:
            break;
    }

    glBindVertexArray(VAO_);
    glBindBuffer(GL_ARRAY_BUFFER, VBO_);
    glBufferData(GL_ARRAY_BUFFER, vertices_.size() * sizeof(PointVertex), vertices_.data(), GL_STATIC_DRAW);

    // Vertex Positions
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(PointVertex), nullptr);
    glBindVertexArray(0);
}

ColliderRenderer::~ColliderRenderer() {
    Draw::inst().unsubscribe(this);
    glDeleteBuffers(1, &VBO_);
    glDeleteVertexArrays(1, &VAO_);
}

void ColliderRenderer::draw(const glm::mat4& viewProjection) {
    // Drawing
    // Always render debug info on top
    glDisable(GL_DEPTH_TEST);

    // MVP
    const glm::mat4 rotation = rotate(glm::mat4(1.0f), rigidBody_->GetTransform().q.GetAngle(), glm::vec3(0, 0, 1));
    const glm::mat4 translation = translate(glm::mat4(1.0f), glm::vec3(rigidBody_->GetPosition().x, rigidBody_->GetPosition().y, 0));
    glm::mat4 mvp = viewProjection * translation * rotation;

    shader_.get()->activate();

    glBindVertexArray(VAO_);
    glUniformMatrix4fv(shader_.get()->getMVPLoc(), 1, false, &mvp[0][0]);
    glDrawArrays(GL_LINE_LOOP, 0, static_cast<GLsizei>(vertices_.size()));
    glBindVertexArray(0);

    shader_.get()->deactivate();
    glEnable(GL_DEPTH_TEST);
}

void ColliderRenderer::enable() {
    Draw::inst().subscribe(this);
}

void ColliderRenderer::disable() {
    Draw::inst().unsubscribe(this);
}

std::vector<PointVertex> ColliderRenderer::generateVertices(const b2PolygonShape* polygon) {
    std::vector<PointVertex> vertices;
    for (int i = 0; i < polygon->m_count; ++i) {
        const auto vert = polygon->m_vertices[i];
        vertices.push_back({ glm::vec3(vert.x, vert.y, 0) });
    }
    return vertices;
}

std::vector<PointVertex> ColliderRenderer::generateVertices(const b2CircleShape* circle) {
    const auto center = circle->m_p;
    const auto radius = circle->m_radius;
    
    const float32 segments = 16.0f;
    const float32 increment = 2.0f * b2_pi / segments;
    const float32 sinInc = sinf(increment);
    const float32 cosInc = cosf(increment);
    b2Vec2 r1(1.0f, 0.0f);
    b2Vec2 v1 = center + radius * r1;

    std::vector<PointVertex> vertices;
    for (int32 i = 0; i < segments; ++i) {
        // Perform rotation to avoid additional trigonometry.
        vertices.push_back({ glm::vec3(v1.x, v1.y, 0 )});
        b2Vec2 r2;
        r2.x = cosInc * r1.x - sinInc * r1.y;
        r2.y = sinInc * r1.x + cosInc * r1.y;
        r1 = r2;
        v1 = center + radius * r2;
    }

    return vertices;
}

std::vector<PointVertex> ColliderRenderer::generateVertices(const b2EdgeShape* edge) {
    std::vector<PointVertex> vertices;

    vertices.push_back({ glm::vec3(edge->m_vertex0.x, edge->m_vertex0.y, 0) });
    vertices.push_back({ glm::vec3(edge->m_vertex1.x, edge->m_vertex1.y, 0) });

    return vertices;
}

std::vector<PointVertex> ColliderRenderer::generateVertices(const b2ChainShape*) {
    throw std::exception("Chain drawing not supported yet!");
}

}

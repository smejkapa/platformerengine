#pragma once
#include "Entity.hpp"
#include "DebugRenderer.hpp"
#include "ResourceProxies.hpp"
#include "Box2D/Dynamics/b2Body.h"
#include "Box2D/Collision/Shapes/b2ChainShape.h"

namespace pe {

// This should probably derive from Renderer
class ColliderRenderer : public DebugRenderer {
private:
    Entity* entity_;
    b2Body* rigidBody_;

    Shader shader_;

    std::vector<PointVertex> vertices_;

    GLuint VAO_;
    GLuint VBO_;
public:
    ColliderRenderer(Entity* entity, b2Body* rigidBody);
    ~ColliderRenderer();

    ColliderRenderer(const ColliderRenderer&) = delete;
    ColliderRenderer& operator=(const ColliderRenderer&) = delete;
    ColliderRenderer(ColliderRenderer&&) = delete;
    ColliderRenderer& operator=(ColliderRenderer&&) = delete;

    void draw(const glm::mat4& viewProjection) override;

protected:
    void enable() override;
    void disable() override;

private:
    std::vector<PointVertex> generateVertices(const b2PolygonShape* polygon);
    std::vector<PointVertex> generateVertices(const b2CircleShape * circle);
    std::vector<PointVertex> generateVertices(const b2EdgeShape* edge);
    std::vector<PointVertex> generateVertices(const b2ChainShape* chain);
};

}

#include "Color.hpp"

namespace pe {

// Color definitions
const Color Color::RED     = Color(1.f, 0.f, 0.f);
const Color Color::GREEN   = Color(0.f, 1.f, 0.f);
const Color Color::BLUE    = Color(0.f, 0.f, 1.f);
const Color Color::CAYAN   = Color(0.f, 1.f, 1.f);
const Color Color::MAGENTA = Color(1.f, 0.f, 1.f);
const Color Color::YELLOW  = Color(1.f, 1.f, 0.f);

}

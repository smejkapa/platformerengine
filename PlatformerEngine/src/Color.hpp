#pragma once

namespace pe {

struct Color {
    float r;
    float g;
    float b;

    Color()
            : Color(0.f, 0.f, 0.f) {
    }

    Color(float r, float g, float b)
            : r(r)
            , g(g)
            , b(b) {
    }

    // Basic color constants
    static const Color RED;
    static const Color GREEN;
    static const Color BLUE;
    static const Color CAYAN;
    static const Color MAGENTA;
    static const Color YELLOW;
};

}

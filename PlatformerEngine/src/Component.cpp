#include "Component.hpp"

namespace pe {
bool Component::getIsActive() const {
    return isActive_;
}

void Component::setIsActive(const bool isActive) {
    isActive_ = isActive;
    if (isActive) {
        enable();
    } else {
        disable();
    }
}
}

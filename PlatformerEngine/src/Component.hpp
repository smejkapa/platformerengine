#pragma once

namespace pe {

class Component {
    bool isActive_ = true;

protected:
    virtual void enable() = 0;
    virtual void disable() = 0;

public:
    virtual ~Component() = default;

    bool getIsActive() const;
    void setIsActive(bool isActive);

};
}

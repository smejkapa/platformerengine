#pragma once
#include "IMessageHandler.hpp"

namespace pe {
namespace util {

class Console : public IMessageHandler {
public:
    void handleMessage(const Message&) override;
};

}
}

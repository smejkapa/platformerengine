#pragma once
#include "Component.hpp"

namespace pe {

class DebugRenderer : public Component {
public:
    virtual void draw(const glm::mat4& viewProjection) = 0;
};
}

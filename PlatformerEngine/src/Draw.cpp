#include "Draw.hpp"

#include <map>

#include "CameraManager.hpp"
#include "MessagePayloadImpl.hpp"

namespace pe {

void Draw::recomputeBatches() {
    batches_.clear();

    for (const auto& renderer : renderers_) {
        auto id = renderer->getId();
        if (batches_.find(id) == batches_.end()) {
            batches_.insert(std::make_pair(id, rend_vec_t{ renderer }));
        } else {
            batches_[id].insert(renderer);
        }
    }
}

void Draw::subscribe(Renderer* renderer) {
    if (renderer->isInstanced()) {
        if (renderers_.find(renderer) == renderers_.end()) {
            renderers_.insert(renderer);
            const auto id = renderer->getId();
            if (batches_.find(id) == batches_.end()) {
                batches_.insert(std::make_pair(id, rend_vec_t{ renderer }));
            } else {
                batches_[id].insert(renderer);
            }
        }
    } else {
        renderersNotInstanced_.insert(renderer);
    }
}

void Draw::unsubscribe(Renderer* renderer) {
    if (renderer->isInstanced()) {
        renderers_.erase(renderer);
        const auto id = renderer->getId();
        if (batches_.find(id) != batches_.end() &&
            batches_[id].find(renderer) != batches_[id].end()) {
            batches_[id].erase(renderer);
            if (batches_[id].empty()) {
                batches_.erase(id);
            }
        }
    } else {
        renderersNotInstanced_.erase(renderer);
    }
}

void Draw::subscribe(DebugRenderer* renderer) {
    debugRenderers_.insert(renderer);
}

void Draw::unsubscribe(DebugRenderer* renderer) {
    debugRenderers_.erase(renderer);
}

void Draw::draw() {
    assert(CameraManager::inst().getMainCamera() != nullptr);
    const auto vp = CameraManager::inst().getMainCamera()->getViewMatrix();

    // Instanced rendering
    for (const auto& b : batches_) {
        inst_mat_vec_t matrices;
        for (auto r : b.second) {
            matrices.push_back(r->getModelMatrix());
        }

        // Take first renderer - no particular reason, assume we have at least one
        (*b.second.begin())->drawInstanced(vp, matrices);
    }

    // Non instanced rendering
    for (auto r : renderersNotInstanced_) {
        r->draw(vp);
    }

    if (isDebugDrawEnabled_) {
        debugDraw();
    }
}

void Draw::debugDraw() {
    const auto vp = CameraManager::inst().getMainCamera()->getViewMatrix();
    for (const auto& debugRenderer : debugRenderers_) {
        debugRenderer->draw(vp);
    }
}

void Draw::handleMessage(const Message& message) {
    if (message.type == Message::Type::RendererChange) {
        //auto r = dynamic_cast<RendererChangePl*>(message.payload.get())->renderer;
        //renderers_.find(r);
        recomputeBatches();
    }
}

void Draw::setIsDebugDrawEnabled(const bool isEnabled) {
    isDebugDrawEnabled_ = isEnabled;
}

bool Draw::getIsDebugDrawEnabled() const {
    return isDebugDrawEnabled_;
}

}

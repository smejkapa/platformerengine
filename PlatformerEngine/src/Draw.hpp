#pragma once
#include "GL/glew.h" // Glew first I guess

#include "glm/glm.hpp"

#include "Singleton.hpp"
#include "System.hpp"
#include "Renderer.hpp"
#include "IMessageHandler.hpp"

#include <unordered_set>
#include <map>
#include "DebugRenderer.hpp"

namespace pe {

class Renderer;

class Draw
        : public Singleton<Draw>
        , public System
        , public IMessageHandler {
    friend Singleton<Draw>;

    using inst_mat_vec_t = std::vector<glm::mat4>;
    using rend_vec_t = std::unordered_set<Renderer*>;
    using batches_t = std::map<renderer_id_t, rend_vec_t>; // Using map because unordered_map needs hashing but tuple does not support it

    batches_t batches_;
    std::unordered_set<Renderer*> renderers_;
    std::unordered_set<Renderer*> renderersNotInstanced_;
    std::unordered_set<DebugRenderer*> debugRenderers_;

    bool isDebugDrawEnabled_ = true;

    Draw() = default;

    void recomputeBatches();
    void debugDraw();

public:
    void subscribe(Renderer*);
    void unsubscribe(Renderer*);
    void subscribe(DebugRenderer*);
    void unsubscribe(DebugRenderer*);

    void draw();

    void handleMessage(const Message& message) override;

    void setIsDebugDrawEnabled(bool isEnabled);
    bool getIsDebugDrawEnabled() const;
};

}

#pragma once

namespace pe {

class Element {
public:
    virtual ~Element() = default;
    virtual void update() = 0;
};

}

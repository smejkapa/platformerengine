#include "Engine.hpp"

#include "game/Character.hpp"
#include "Settings.hpp"
#include "util/FpsDisplay.hpp"
#include "util/Input.hpp"
#include "util/Time.hpp"
#include "Draw.hpp"

#include "GLFW/glfw3.h"

#include <iostream>
#include "game/TerrariaGame.hpp"

#include "Physics.hpp"
#include "Update.hpp"

namespace pe {

Engine::Engine()
        : window_(nullptr) {
    // First of all init openGL, other classes may want to use it
    if (initOpenGL() != 0) {
        exit(-1);
    }

    Physics::inst();

    util::Input::inst().setWindow(window_);
    game_ = std::make_unique<tg::TerrariaGame>();
}

int Engine::initOpenGL() {
    // Initialize the library
    if (!glfwInit()) {
        return -1;
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

    if (Settings::IsMultisamplingEnabled) {
        glfwWindowHint(GLFW_SAMPLES, Settings::MultisamplingLevel);
    }
    glEnable(Settings::IsMultisamplingEnabled);

    // Create a windowed mode window_ and its OpenGL context
    window_ = glfwCreateWindow(
        Settings::ScreenWidth,
        Settings::ScreenHeight,
        "Platformer engine",
        nullptr,
        nullptr
    );

    if (!window_) {
        std::cout << "Failed to open GLFW window_" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window_);

    // Initialize GLEW
    glewExperimental = GL_TRUE; // Needed for core profile
    if (glewInit() != GLEW_OK) {
        std::cout << "Failed to initialize GLEW" << std::endl;
        return -1;
    }

    // Setup viewport inside the window_
    int width, height;
    glfwGetFramebufferSize(window_, &width, &height);
    glViewport(0, 0, width, height);

    // Enable depth test
    glEnable(GL_DEPTH_TEST);
    // Accept fragment if it closer to the camera than the former one
    glDepthFunc(GL_LESS);

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glfwSwapInterval(static_cast<int>(Settings::Vsync));

    glClearColor(0.53f, 0.63f, 0.97f, 0.0f);

    return 0;
}

int Engine::run() {
    game_->init();

    util::FpsDisplay fpsDisplay;

    bool wireframe = false;

    const auto vendor = glGetString(GL_VENDOR);
    std::cout << std::endl << "Running using gpu: " << vendor << std::endl;

    printInputMapping();

    // Main loop
    while (!glfwWindowShouldClose(window_)
            && glfwGetKey(window_, GLFW_KEY_ESCAPE) != GLFW_PRESS) {
        glfwPollEvents();

        // Update
        util::Time::inst().update();
        // Handle input
        util::Input::inst().update();
        if (util::Input::inst().getKeyDown(util::KeyCode::F)) {
            fpsDisplay.toggle();
        }
        if (util::Input::inst().getKeyDown(util::KeyCode::O)) {
            wireframe = !wireframe;
            if (wireframe)
                glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            else
                glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }
        if (util::Input::inst().getKeyDown(util::KeyCode::L)) {
            Draw::inst().setIsDebugDrawEnabled(!Draw::inst().getIsDebugDrawEnabled());
        }

        Physics::inst().update();
        
        Update::inst().update();
        game_->update();

        // Draw
        fpsDisplay.showFps(window_);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        Draw::inst().draw();

        const auto glError = glGetError();
        if (glError != GL_NO_ERROR) {
            std::cerr << "OpenGL ERROR: " << glError << std::endl;
        }

        glfwSwapBuffers(window_);
    }

    shutDown();

    glfwTerminate();
    return 0;
}

void Engine::shutDown() const {
    util::Input::inst().shutDown();
}

void Engine::printInputMapping() {
    std::cout << "Input mapping:" << std::endl;

    std::cout << "F: toggle FPS output" << std::endl;
    std::cout << "O: toggle wireframe drawing" << std::endl;
    std::cout << "L: toggle collider drawing" << std::endl;

    std::cout << std::endl;
}

}

#pragma once

#include "GL/glew.h" // Glew first I guess

#include <memory>
#include <GLFW/glfw3.h>

#include "IGame.hpp"
#include "Console.hpp"

namespace pe {

class Engine {
    GLFWwindow* window_;
    std::unique_ptr<IGame> game_;
    util::Console console_;

    int initOpenGL();
    void printInputMapping();

public:
    Engine();
    int run();
    void shutDown() const;
};

}

#include "Entity.hpp"
#include <utility>
#include "Update.hpp"

namespace pe {

Entity::Entity() {
    transform_.setEntity(this);
    Update::inst().subscribe(this);
}

Entity::Entity(Transform transform)
        : transform_(std::move(transform)) {
    transform_.setEntity(this);
    Update::inst().subscribe(this);
}

Entity::~Entity() {
    Update::inst().unsubscribe(this);
}

const Transform& Entity::getTransform() const {
    return transform_;
}

Transform& Entity::getTransform() {
    return transform_;
}

void Entity::moveTo(const glm::vec2& newPosition) {
    transform_.setPosition(newPosition);
}

void Entity::move(const glm::vec2& delta) {
    moveTo(transform_.getPosition() + delta);
}

void Entity::update() {
}

bool Entity::getIsActive() const {
    return isActive_;
}

void Entity::setIsActive(const bool isActive) {
    isActive_ = isActive;

    if (isActive_) {
        Update::inst().subscribe(this);
    } else {
        Update::inst().unsubscribe(this);
    }

    for(const auto& component : components_) {
        component->setIsActive(isActive);
    }

    const auto& children = transform_.getChildren();
    for (auto& child : children) {
        auto en = child->getEntity();
        en->setIsActive(isActive);
    }
}

}

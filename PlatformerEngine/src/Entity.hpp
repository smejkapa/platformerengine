#pragma once

#include "IUpdatable.hpp"
#include "data_structures/Transform.hpp"
#include "Component.hpp"

#include <vector>
#include <memory>

namespace pe {

/** Basic class representing objects in scene */
class Entity :
        public IUpdatable {
private:
    bool isActive_ = true;

protected:
    std::vector<std::unique_ptr<Component>> components_;
    Transform transform_;

public:
    Entity();
    explicit Entity(Transform transform);

    virtual ~Entity();

    // TODO implement copy/move operators
    Entity(const Entity&) = delete;
    Entity& operator=(const Entity&) = delete;
    Entity(Entity&&) = delete;
    Entity& operator=(Entity&&) = delete;

    const Transform& getTransform() const;
    Transform& getTransform();

    virtual void moveTo(const glm::vec2& newPosition);
    virtual void move(const glm::vec2& delta);

    void update() override;

    bool getIsActive() const;
    void setIsActive(bool isActive);

    template<typename TComponent>
    TComponent* addComponent(std::unique_ptr<TComponent>&&);

    template<typename TComponent, typename ...TArgs>
    TComponent* addComponent(TArgs... args);
};

template <typename TComponent>
TComponent* Entity::addComponent(std::unique_ptr<TComponent>&& component) {
    const auto ptr = component.get();
    components_.push_back(std::move(component));
    return ptr;
}

template <typename TComponent, typename ... TArgs>
TComponent* Entity::addComponent(TArgs... args) {
    components_.emplace_back(std::make_unique<TComponent>(std::forward<TArgs>(args)...));
    return dynamic_cast<TComponent*>(components_.back().get());
}

}

#pragma once

namespace pe {

class IDrawable {
public:
    virtual ~IDrawable() = default;
    virtual void draw() = 0;
};

}

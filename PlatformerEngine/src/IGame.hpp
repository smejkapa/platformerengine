#pragma once

#include "Message.hpp"
#include "IMessageHandler.hpp"

namespace pe {

class IGame : public IMessageHandler {
public:
    virtual ~IGame() = default;
    virtual void init() = 0;
    virtual void update() = 0;
};

}

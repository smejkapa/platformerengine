#include "IMessageHandler.hpp"
#include "MessageBus.hpp"

namespace pe {

IMessageHandler::IMessageHandler() {
    MessageBus::inst().subscribe(this);
}

IMessageHandler::~IMessageHandler() {
    MessageBus::inst().unsubscribe(this);
}

}

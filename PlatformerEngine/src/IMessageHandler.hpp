#pragma once
#include "Message.hpp"

namespace pe {

class IMessageHandler {
public:
    IMessageHandler();
    virtual ~IMessageHandler();

    virtual void handleMessage(const Message&) = 0;
};

}

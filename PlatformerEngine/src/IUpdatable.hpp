#pragma once

namespace pe {

class IUpdatable {
public:
    virtual ~IUpdatable() = default;
    virtual void update() = 0;
};

}

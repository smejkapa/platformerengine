#include "Material.hpp"

namespace pe {

Material::id_t Material::makeId() const {
    Texture_r::id_t texId{};
    if (texture_.get())
        texId = texture_.get()->getId();

    return std::make_pair(shader_.get()->getId(), texId);
}

Material::Material(const Shader& shader, const Texture& texture)
        : shader_(shader)
        , texture_(texture) {
    // Make id here to emphasize the need for full initialization before making id
    id_ = makeId();
}

void Material::activate() const {
    shader_.get()->activate();
    if (texture_.get())
        texture_.get()->activate();
}

void Material::deactivate() const {
    if (texture_.get())
        texture_.get()->deactivate();
    shader_.get()->deactivate();
}

const Material::id_t& Material::getId() const {
    return id_;
}

}

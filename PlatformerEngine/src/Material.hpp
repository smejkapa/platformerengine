#pragma once
#include "ResourceProxies.hpp"
#include "Shader_r.hpp"
#include "Texture_r.hpp"

namespace pe {

class Material {
public:
    using id_t = std::pair<Shader_r::id_t, Texture_r::id_t>;

private:

    Shader shader_;
    Texture texture_;

    id_t id_;

    id_t makeId() const;

public:
    Material(const Shader& shader, const Texture& texture);

    void activate() const;
    void deactivate() const;

    bool isInstanced() const {
        return shader_.get()->getInstanceMVPLoc() != Shader_r::LOCATION_NONE;
    }

    const id_t& getId() const;

    Shader getShader() const { return shader_; }
    Texture getTexture() const { return texture_; }
};

}

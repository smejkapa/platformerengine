#include "Message.hpp"
#include <algorithm>

namespace pe {

Message::Message(Message&& other) noexcept
        : payload(nullptr) {
    *this = std::move(other);
}

Message& Message::operator=(Message&& other) noexcept {
    if (this != &other) {
        type = std::move(other.type);
        payload = std::move(other.payload);
        other.payload = nullptr;
    }

    return *this;
}

Message::~Message() {
    if (payload)
        payload->returnToPool();
}

}

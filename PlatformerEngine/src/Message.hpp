#pragma once
#include "MessagePayload.hpp"

namespace pe {

struct Message {
    enum class Type {
        None, // Just in case we forget to init message
        ButtonReleased,
        ButtonDown,
        Action,
        RendererChange
    };

    Type type;
    MessagePayload* payload;

    explicit Message(Type type)
            : type(type)
            , payload(nullptr) {
    }

    Message(Message&& other) noexcept;
    Message& operator=(Message&& other) noexcept;

    virtual ~Message();
};

}

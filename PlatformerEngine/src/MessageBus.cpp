#include "MessageBus.hpp"

namespace pe {

void MessageBus::sendMessage(const Message& m) {
    for (auto& h : handlers_) {
        h->handleMessage(m);
    }
}

void MessageBus::subscribe(IMessageHandler* handler) {
    // Add only if not present already
    if (handlers_.find(handler) == handlers_.end())
        handlers_.insert(handler);
}

void MessageBus::unsubscribe(IMessageHandler* handler) {
    handlers_.erase(handler);
}

}

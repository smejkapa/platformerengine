#pragma once
#include "Message.hpp"
#include "IMessageHandler.hpp"
#include <unordered_set>
#include "Singleton.hpp"

namespace pe {

class MessageBus : public Singleton<MessageBus> {
    friend Singleton<MessageBus>;

    std::unordered_set<IMessageHandler*> handlers_;

    MessageBus() = default;

public:
    MessageBus(const MessageBus&) = delete;
    MessageBus& operator=(const MessageBus&) = delete;

    void sendMessage(const Message&);
    void subscribe(IMessageHandler*);
    void unsubscribe(IMessageHandler*);
};

}

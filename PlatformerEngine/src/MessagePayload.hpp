#pragma once

namespace pe {

/**
 * \brief Base class for message payload
 */
struct MessagePayload {
    virtual ~MessagePayload() = default;
    virtual void returnToPool() = 0;
};

}

#include "MessagePayloadImpl.hpp"

namespace pe {

void InputPl::returnToPool() {
    PoolManager::inst().getInputPool().giveBack(this);
}

void InputPl::reset() {
    Key = 0;
    Scancode = 0;
    Mode = 0;
    IsJustPressed = false;
}

void InputPl::set(int key, int scancode, int mode, bool isJustPressed) {
    this->Key = key;
    this->Scancode = scancode;
    this->Mode = mode;
    this->IsJustPressed = isJustPressed;
}

InputPl::InputPl() {
    reset();
}


void ActionPl::returnToPool() {
    PoolManager::inst().getActionPool().giveBack(this);
}

void ActionPl::reset() {
    Action = Type::None;
}

void ActionPl::set(Type action) {
    this->Action = action;
}

ActionPl::ActionPl() {
    reset();
}


void RendererChangePl::returnToPool() {
    PoolManager::inst().getRendererChangePool().giveBack(this);
}

void RendererChangePl::reset() {
    Renderer = nullptr;
}

void RendererChangePl::set(Renderer_t* renderer) {
    this->Renderer = renderer;
}

RendererChangePl::RendererChangePl() {
    reset();
}

}

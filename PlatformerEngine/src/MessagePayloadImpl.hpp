#pragma once
#include "MessagePayload.hpp"
#include "PoolManager.hpp"

namespace pe {

struct InputPl
        : MessagePayload {
    friend ObjectPool<InputPl>;

    int Key;
    int Scancode;
    int Mode;
    bool IsJustPressed;

    // Factory
    template <typename ... Types>
    static InputPl* create(Types ... args) {
        return PoolManager::inst().getInputPool().acquire(std::forward<Types>(args)...);
    }

    // Pool methods
    void returnToPool() override;
    void reset();
    void set(int key, int scancode, int mode, bool isJustPressed = false);

private:
    InputPl();
};


struct ActionPl
        : MessagePayload {
    friend ObjectPool<ActionPl>;

    enum class Type {
        None,
        MoveLeft,
        MoveRight,
        MoveDown,
        MoveUp
    };

    Type Action;

    // Factory
    template <typename ... Types>
    static ActionPl* create(Types ... args) {
        return PoolManager::inst().getActionPool().acquire(std::forward<Types>(args)...);
    }

    // Pool methods
    void returnToPool() override;
    void reset();
    void set(Type action);

private:
    ActionPl();
};


class Renderer;

struct RendererChangePl
        : MessagePayload {
    friend ObjectPool<RendererChangePl>;

    using Renderer_t = Renderer;

    Renderer_t* Renderer;

    // Factory
    template <typename ... Types>
    static RendererChangePl* create(Types ... args) {
        return PoolManager::inst().getRendererChangePool().acquire(std::forward<Types>(args)...);
    }

    // Pool methods
    void returnToPool() override;
    void reset();
    void set(Renderer_t* renderer);

private:
    RendererChangePl();
};

}

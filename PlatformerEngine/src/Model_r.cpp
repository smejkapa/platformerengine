#include "Model_r.hpp"

#include "Material.hpp"

#include <iostream>

#include <SOIL/SOIL.h>
#include "Settings.hpp"


namespace pe {

// =======
// Loading 
// =======
void Model_r::processMesh(aiMesh* mesh, const aiScene*) {
    // Data to fill
    std::vector<Vertex> vertices;
    std::vector<GLuint> indices;

    // Walk through each of the mesh's vertices
    for (GLuint i = 0; i < mesh->mNumVertices; i++) {
        Vertex vertex;
        glm::vec3 vector;
        // Positions
        vector.x = mesh->mVertices[i].x;
        vector.y = mesh->mVertices[i].y;
        vector.z = mesh->mVertices[i].z;
        vertex.Position = vector;
        // Normals
        if (mesh->mNormals) {
            vector.x = mesh->mNormals[i].x;
            vector.y = mesh->mNormals[i].y;
            vector.z = mesh->mNormals[i].z;
            vertex.Normal = vector;
        } else {
            vertex.Normal = glm::vec3();
        }
        // Texture Coordinates
        if (mesh->mTextureCoords[0]) {
            glm::vec2 vec;
            vec.x = mesh->mTextureCoords[0][i].x;
            vec.y = mesh->mTextureCoords[0][i].y;
            vertex.TexCoord = vec;
        } else {
            vertex.TexCoord = glm::vec2(0.0f, 0.0f);
        }
        vertices.push_back(vertex);
    }
    for (GLuint i = 0; i < mesh->mNumFaces; i++) {
        aiFace face = mesh->mFaces[i];
        for (GLuint j = 0; j < face.mNumIndices; j++)
            indices.push_back(face.mIndices[j]);
    }

    setupModel(vertices, indices);
}

void Model_r::setupModel(const std::vector<Vertex>& vertices, const std::vector<GLuint>& indices) {
    elementCount_ = static_cast<GLsizei>(indices.size());

    glGenVertexArrays(1, &VAO_);
    glGenBuffers(1, &VBO_);
    glGenBuffers(1, &EBO_);
    glGenBuffers(1, &instMatrix_);

    glBindVertexArray(VAO_);

    glBindBuffer(GL_ARRAY_BUFFER, VBO_);
    glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(Vertex), vertices.data(), GL_STATIC_DRAW);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO_);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(GLuint), indices.data(), GL_STATIC_DRAW);

    // Vertex Positions
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), nullptr);
    // Vertex Normals
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<GLvoid*>(offsetof(Vertex, Normal)));
    // Vertex Texture Coords
    glEnableVertexAttribArray(2);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), reinterpret_cast<GLvoid*>(offsetof(Vertex, TexCoord)));

    glBindVertexArray(0);
}

void Model_r::loadModel(const std::string& id) {
    Assimp::Importer import;
    auto path = Settings::ASSET_FOLDER + id;
    const aiScene* scene = import.ReadFile(path, aiProcess_Triangulate | aiProcess_FlipUVs);

    if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode) {
        std::cout << "ERROR::ASSIMP::" << import.GetErrorString() << std::endl;
        return;
    }

    processNode(scene->mRootNode, scene);
}

void Model_r::processNode(aiNode* node, const aiScene* scene) {
    assert(node->mNumMeshes < 2);
    for (GLuint i = 0; i < node->mNumMeshes; i++) {
        aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
        processMesh(mesh, scene);
    }

    for (GLuint i = 0; i < node->mNumChildren; i++) {
        processNode(node->mChildren[i], scene);
    }
}

// =======
// Drawing
// =======
void Model_r::draw(const glm::mat4& mvp, GLint mvpLoc, const Material& material) const {
    material.activate();
    glBindVertexArray(VAO_);

    glUniformMatrix4fv(mvpLoc, 1, false, &mvp[0][0]);
    glDrawElements(GL_TRIANGLES, elementCount_, GL_UNSIGNED_INT, nullptr);

    glBindVertexArray(0);
    material.deactivate();
}

void Model_r::drawInstanced(
    const glm::mat4&              vp,
    const GLint                   mvpLoc,
    const std::vector<glm::mat4>& instanceMatrices,
    const GLint                   mvpInstLoc,
    const Material&               material
) const {
    material.activate();
    glBindVertexArray(VAO_);

    // Setup instance matrices
    glBindBuffer(GL_ARRAY_BUFFER, instMatrix_);
    glBufferData(GL_ARRAY_BUFFER, instanceMatrices.size() * sizeof(glm::mat4), instanceMatrices.data(), GL_STATIC_DRAW);
    for (int i = 0; i < 4; i++) {
        // Set up the vertex attribute
        glVertexAttribPointer(mvpInstLoc + i, 4, GL_FLOAT, GL_FALSE, sizeof(glm::mat4), reinterpret_cast<void*>(sizeof(glm::vec4) * i));
        // Enable it
        glEnableVertexAttribArray(mvpInstLoc + i);
        // Make it instanced
        glVertexAttribDivisor(mvpInstLoc + i, 1);
    }

    glUniformMatrix4fv(mvpLoc, 1, false, &vp[0][0]);
    glDrawElementsInstanced(GL_TRIANGLES, elementCount_, GL_UNSIGNED_INT, nullptr, static_cast<GLsizei>(instanceMatrices.size()));

    glBindVertexArray(0);
    material.deactivate();
}

}

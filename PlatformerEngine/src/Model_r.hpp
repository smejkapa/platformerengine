#pragma once
#include <GL/glew.h>

#include "Vertex.hpp"

#include <vector>

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include "Resource.hpp"

namespace pe {
    
class Material;

class Model_r
        : public Resource<std::string> {

    GLuint VAO_;
    GLuint VBO_;
    GLuint EBO_;
    GLuint instMatrix_;
    GLsizei elementCount_;

    // =======
    // Loading
    // =======
    void loadModel(const std::string& id);
    void processNode(aiNode* node, const aiScene* scene);
    void processMesh(aiMesh* mesh, const aiScene* scene);
    void setupModel(const std::vector<Vertex>& vertices, const std::vector<GLuint>& indices);

public:
    explicit Model_r(const id_t& id)
            : Resource<id_t>(id) {
        loadModel(id);
    }

    ~Model_r() override {
        if (!isMoved()) {
            glDeleteBuffers(1, &instMatrix_);
            glDeleteBuffers(1, &EBO_);
            glDeleteBuffers(1, &VBO_);
            glDeleteVertexArrays(1, &VAO_);
        }
    }

    Model_r(const Model_r&) = delete;
    Model_r& operator=(const Model_r&) = delete;

    Model_r(Model_r&& other) noexcept {
        *this = std::move(other);
    }

    Model_r& operator=(Model_r&& other) noexcept {
        if (this != &other) {
            VAO_ = std::move(other.VAO_);
            EBO_ = std::move(other.EBO_);
            instMatrix_ = std::move(other.instMatrix_);
            elementCount_ = std::move(other.elementCount_);

            Resource<id_t>::operator=(std::move(other));
        }

        return *this;
    }


    // =======
    // Drawing
    // =======
    void draw(const glm::mat4& mvp, GLint mvpLoc, const Material& material) const;
    void drawInstanced(const glm::mat4& vp, GLint mvpLoc, const std::vector<glm::mat4>& instanceMatrices, GLint mvpInstLoc, const Material& material) const;
};

}

#pragma once
#include <memory>
#include <stack>
#include <vector>

namespace pe {

template <typename T>
class ObjectPool {
    static constexpr size_t DEFAULT_SIZE = 8;

    const size_t stepSize_;
    std::vector<std::unique_ptr<T[]>> pools_;
    std::stack<T*> freeList_;

    void addObjects(size_t count) {
        pools_.push_back(std::unique_ptr<T[]>(new T[count]));
        for (size_t i = 0; i < count; ++i) {
            freeList_.push(&pools_[pools_.size() - 1][i]);
        }
    }

public:
    ObjectPool()
            : ObjectPool(DEFAULT_SIZE) {
    }

    explicit ObjectPool(size_t stepSize)
            : stepSize_(stepSize) {
        addObjects(stepSize_);
    }

    virtual ~ObjectPool() = default;

    template <typename ... Types>
    T* acquire(Types&& ... args) {
        if (freeList_.empty()) {
            addObjects(stepSize_);
        }

        auto top = freeList_.top();
        freeList_.pop();
        top->reset();
        top->set(std::forward<Types>(args)...);
        return top;
    }

    void giveBack(T* object) {
        if (object != nullptr)
            freeList_.push(std::move(object));
    }
};

}

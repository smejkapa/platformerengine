﻿#include "OrthoCamera.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "Settings.hpp"

namespace pe {

OrthoCamera::OrthoCamera()
        : width_(20.0f)
        , height_(20.0f) {
    transform_.setPosition(glm::vec3(0.0f, 0.0f, 10.0f));
    setFrontVector(glm::vec3(0.0f, 0.0f, -1.0f));

    setUpVector(glm::vec3(0.f, 1.f, 0.f));
    setNear(0.0f);
    setFar(100.0f);
}

glm::mat4 OrthoCamera::getViewMatrix() const {
    const float aspect = static_cast<float>(Settings::ScreenWidth) / static_cast<float>(Settings::ScreenHeight);

    auto left = -width_ / 2 * aspect;
    auto right = -left;
    auto bottom = -height_ / 2;
    auto top = -bottom;

    auto lookAt = transform_.getPosition3() + frontVector_;

    glm::mat4 projection = glm::ortho(left, right, bottom, top, near_, far_);
    glm::mat4 view = glm::lookAt(transform_.getPosition3(), lookAt, upVector_);

    return projection * view;
}

void OrthoCamera::update() {
}

}

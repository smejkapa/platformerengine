﻿#pragma once
#include "Camera.hpp"

namespace pe {

class OrthoCamera : public Camera {
private:
    float width_;
    float height_;

public:
    OrthoCamera();
    virtual ~OrthoCamera() = default;
    glm::mat4 getViewMatrix() const override;

    float getWidth() const { return width_; }
    float getHeight() const { return height_; }

    void setWidth(float width) { width_ = width; }
    void setHeight(float height) { height_ = height; }

    void update() override;
};

}

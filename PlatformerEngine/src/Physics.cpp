#include "Physics.hpp"

#include "util/Time.hpp"

namespace pe {

Physics::Physics()
        : gravity_(0.0f, -10.0f)
        , world_(gravity_)
        , timeStep_(WORLD_STEP_PERIOD)
        , velocityIterCount_(6)
        , positionIterCount_(2)
        , timeToNextWorldStep_(0.0f) {
}

const b2World& Physics::getWorld() const {
    return world_;
}

b2World& Physics::getWorld() {
    return world_;
}

void Physics::setGravity(const b2Vec2& gravity) {
    gravity_ = gravity;
    world_.SetGravity(gravity_);
}

void Physics::update() {
    // Move colliders according to their transforms
    for (const auto& rigidBody : rigidBodies_) {
        rigidBody->alignWithTransform();
    }

    worldStep();
}

void Physics::worldStep() {
    timeToNextWorldStep_ -= util::Time::inst().delta();

    if (timeToNextWorldStep_ > 0.0f) {
        return;
    }

    timeToNextWorldStep_ = WORLD_STEP_PERIOD;
    world_.Step(timeStep_, velocityIterCount_, positionIterCount_);

    // Apply physical movement to things
    for (const auto& rigidBody : rigidBodies_) {
        if (rigidBody->getRigidBody()->GetType() != b2_staticBody) {
            rigidBody->updateTransform();
        }
    }
}

void Physics::subscribe(RigidBody* rigidBody) {
    rigidBodies_.insert(rigidBody);
}

void Physics::unsubscribe(RigidBody* rigidBody) {
    rigidBodies_.erase(rigidBody);
}

}

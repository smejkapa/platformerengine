#pragma once
#include "Singleton.hpp"
#include "System.hpp"

#include "Box2D/Box2D.h"
#include "RigidBody.hpp"
#include <unordered_set>

namespace pe {

class Physics
        : public Singleton<Physics>
        , public System {
    friend Singleton <Physics>;

private:
    b2Vec2 gravity_;
    b2World world_;
    float timeStep_;
    int velocityIterCount_;
    int positionIterCount_;

    static constexpr float WORLD_STEP_PERIOD = 1.0f / 60.0f;
    float timeToNextWorldStep_;

    std::unordered_set<RigidBody*> rigidBodies_;

public:
    Physics();

    const b2World& getWorld() const;
    b2World& getWorld();

    void setGravity(const b2Vec2& gravity);

    void update();
    void worldStep();

    void subscribe(RigidBody*);
    void unsubscribe(RigidBody*);
};
}

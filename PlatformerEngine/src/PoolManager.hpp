#pragma once
#include "ObjectPool.hpp"
#include "MessagePayloadImpl.hpp"
#include "Singleton.hpp"
#include <iostream>

namespace pe {
    
struct InputPl;
struct ActionPl;
struct RendererChangePl;

class PoolManager
        : public Singleton<PoolManager> {
    friend Singleton<PoolManager>;

    ObjectPool<InputPl> inputPlPool_;
    ObjectPool<ActionPl> actionPlPool_;
    ObjectPool<RendererChangePl> rendererChangePool_;

public:
    ObjectPool<InputPl>& getInputPool() { return inputPlPool_; }
    ObjectPool<ActionPl>& getActionPool() { return actionPlPool_; }
    ObjectPool<RendererChangePl>& getRendererChangePool() { return rendererChangePool_; }
};

}

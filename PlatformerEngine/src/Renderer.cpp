#include "Renderer.hpp"
#include <utility>
#include "Draw.hpp"

namespace pe {

void Renderer::enable() {
    Draw::inst().subscribe(this);
}

void Renderer::disable() {
    Draw::inst().unsubscribe(this);
}

Renderer::Renderer(Entity* entity, Material material, const Model_r::id_t& modelId)
        : entity_(entity)
        , material_(std::move(material))
        , model_(ResourceManager::inst().getModel(modelId)) {
    Draw::inst().subscribe(this);
}

Renderer::~Renderer() {
    Draw::inst().unsubscribe(this);
}

Renderer::Renderer(const Renderer& other)
        : Component(other)
        , entity_(other.entity_)
        , material_(other.material_)
        , model_(other.model_) {
    Draw::inst().subscribe(this);
}

Renderer& Renderer::operator=(const Renderer& other) {
    Component::operator=(other);
    
    Draw::inst().unsubscribe(this);

    entity_ = other.entity_;
    material_ = other.material_;
    model_ = other.model_;

    Draw::inst().subscribe(this);
    return *this;
}

glm::mat4 Renderer::getModelMatrix() const {
    return entity_->getTransform().getModelMatrix();
}

const Entity* Renderer::getEntity() const {
    return entity_;
}

std::string Renderer::getModelName() const {
    return model_.get()->getId();
}

renderer_id_t Renderer::getId() const {
    return std::make_tuple(material_.getId(), model_.get()->getId());
}

bool Renderer::isInstanced() const {
    return material_.isInstanced();
}

void Renderer::draw(const glm::mat4& vp) const {
    const auto mvp = vp * entity_->getTransform().getModelMatrix();

    model_.get()->draw(
        mvp, 
        material_.getShader().get()->getMVPLoc(),
        material_
    );
}

void Renderer::drawInstanced(const glm::mat4& vp, const std::vector<glm::mat4>& instanceMatrices) const {
    model_.get()->drawInstanced(
        vp, 
        material_.getShader().get()->getMVPLoc(), 
        instanceMatrices, 
        material_.getShader().get()->getInstanceMVPLoc(), 
        material_
    );
}

}

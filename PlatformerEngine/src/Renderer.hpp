#pragma once
#include "GL/glew.h" // Glew first I guess

#include "Component.hpp"

#include "ResourceProxies.hpp"
#include "Entity.hpp"
#include "Material.hpp"

namespace pe {
using renderer_id_t = std::tuple<Material::id_t, Model_r::id_t>;

class Renderer : public Component {
protected:
    Entity* entity_;
    Material material_;
    Model model_;

    void enable() override;
    void disable() override;

public:
    Renderer(Entity*, Material, const Model_r::id_t&);
    virtual ~Renderer();

    Renderer(const Renderer& other);
    Renderer& operator=(const Renderer& other);

    // TODO implement move operations
    Renderer(Renderer&& other) = delete;
    Renderer& operator=(Renderer&& other) = delete;

    glm::mat4 getModelMatrix() const;
    const Entity* getEntity() const;
    
    std::string getModelName() const;
    renderer_id_t getId() const;

    bool isInstanced() const;

    void draw(const glm::mat4& vp) const;
    void drawInstanced(const glm::mat4& vp, const std::vector<glm::mat4>& instanceMatrices) const;
};
}

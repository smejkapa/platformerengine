#pragma once

#include <algorithm>
#include <utility>

namespace pe {

/**
 * \brief Base class for all unmanaged resources
 * \tparam ID_T Type to use as an ID for this resource type
 */
template <typename ID_T>
class Resource {
public:
    /**
    * \brief Type used as an ID for this resource type
    */
    using id_t = ID_T;

private:
    bool isMoved_;

protected:
    id_t id_;

    /**
     * \brief Indicates if this instance was moved from or not
     * \return Was this instacne moved from?
     */
    bool isMoved() const { return isMoved_; }

public:
    Resource()
            : isMoved_(false)
            , id_(id_t{}) {
    }

    explicit Resource(id_t id)
            : isMoved_(false)
            , id_(std::move(id)) {
    }

    virtual ~Resource() = default;

    // Resources should not be copied since we want them loaded only once
    Resource(const Resource&) = delete;
    Resource& operator=(const Resource&) = delete;

    /* Move operations */
    Resource(Resource&& other) noexcept {
        *this = std::move(other);
    }

    Resource& operator=(Resource&& other) noexcept {
        if (this != &other) {
            id_ = std::move(other.id_);
            isMoved_ = std::move(other.isMoved_);
            other.isMoved_ = true;
        }

        return *this;
    }


    /**
     * \brief ID of this resource
     * \return Unique identifier of this resource
     */
    const id_t& getId() const { return id_; }
};

}

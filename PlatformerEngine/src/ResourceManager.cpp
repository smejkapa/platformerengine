#include "ResourceManager.hpp"
#include "ResourceProxy.hpp"

#if defined(_DEBUG)
#include <iostream>
#endif

namespace pe {

ResourceManager::ResourceManager() {
    // Following section should probably be in a config file
    // Format:   [Vertex shader file]	[Fragment shader file]	 [Instanced MVP location]
    shaderDetails_[Shader_r::id_t::UnlitColorInst]     = std::make_tuple("shaders/UnlitSolid_In.vert", "shaders/UnlitSolid_In.frag", 3);
    shaderDetails_[Shader_r::id_t::UnlitRedInst]       = std::make_tuple("shaders/UnlitSolid_In.vert", "shaders/UnlitRed_In.frag", 3);
    shaderDetails_[Shader_r::id_t::SpriteTextured]     = std::make_tuple("shaders/SpriteTextured.vert", "shaders/SpriteTextured.frag", Shader_r::LOCATION_NONE);
    shaderDetails_[Shader_r::id_t::SpriteTexturedInst] = std::make_tuple("shaders/SpriteTextured_In.vert", "shaders/SpriteTextured.frag", 3);
    shaderDetails_[Shader_r::id_t::ColliderDebugDraw]  = std::make_tuple("shaders/ColliderDebugDraw.vert", "shaders/ColliderDebugDraw.frag", Shader_r::LOCATION_NONE);
}

// =======
// Shaders
// =======
Shader ResourceManager::getShader(const Shader_r::id_t& id) {
    assert(id != Shader_r::id_t{});
    if (shaders_.find(id) == shaders_.end()) {
#if defined(_DEBUG)
		std::cout << "Shader loaded: " << static_cast<int>(id) << std::endl;
#endif
        std::string vs, fs;
        int mvp;
        std::tie(vs, fs, mvp) = shaderDetails_[id];
        shaders_.insert(std::make_pair(id, Shader_r(id, vs, fs, mvp)));
    }

    return Shader(&shaders_.at(id));
}

void ResourceManager::increaseCount(Shader_r* shader) {
    ++shaderCounts_[shader->getId()];
}

void ResourceManager::decreaseCount(Shader_r* shader) {
    const auto& id = shader->getId();
    if (--shaderCounts_[id] <= 0) {
#if defined(_DEBUG)
		std::cout << "Shader unloaded: " << static_cast<int>(id) << std::endl;
		assert(shaders_.find(id) != shaders_.end());
#endif
        // Unload
        shaders_.erase(id);
    }
}


// ======
// Models
// ======
Model ResourceManager::getModel(const Model_r::id_t& id) {
    assert(id != Model_r::id_t{});
    if (models_.find(id) == models_.end()) {
#if defined(_DEBUG)
		std::cout << "Model loaded: " << id << std::endl;
#endif
        models_.insert(std::make_pair(id, Model_r(id)));
    }

    return Model(&models_.at(id));
}

void ResourceManager::increaseCount(Model_r* model) {
    ++modelCounts_[model->getId()];
}

void ResourceManager::decreaseCount(Model_r* model) {
    const auto& id = model->getId();
    if (--modelCounts_[id] <= 0) {
#if defined(_DEBUG)
		std::cout << "Model unloaded: " << id << std::endl;
		assert(models_.find(id) != models_.end());
#endif
        models_.erase(id);
    }
}


// ========
// Textures
// ========
Texture ResourceManager::getTexture(const Texture_r::id_t& id) {
    assert(id != Texture_r::id_t{});
    if (textures_.find(id) == textures_.end()) {
#if defined(_DEBUG)
		std::cout << "Texture loaded: " << id << std::endl;
#endif
        textures_.insert(std::make_pair(id, Texture_r(id)));
    }

    return Texture(&textures_.at(id));
}

void ResourceManager::increaseCount(Texture_r* texture) {
    ++textureCounts_[texture->getId()];
}

void ResourceManager::decreaseCount(Texture_r* texture) {
    const auto& id = texture->getId();
    if (--textureCounts_[id] <= 0) {
#if defined(_DEBUG)
		std::cout << "Texture unloaded: " << id << std::endl;
		assert(textures_.find(id) != textures_.end());
#endif
        textures_.erase(id);
    }
}

}

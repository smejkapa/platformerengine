#pragma once
#include <unordered_map>
#include "Singleton.hpp"
#include "ResourceProxies.hpp"
#include "Model_r.hpp"
#include "Shader_r.hpp"
#include "Texture_r.hpp"

namespace pe {

class ResourceManager
        : public Singleton<ResourceManager> {
    friend Singleton<ResourceManager>;

    /* Shaders */
    std::unordered_map<Shader_r::id_t, Shader_r> shaders_;
    std::unordered_map<Shader_r::id_t, int> shaderCounts_;
    std::unordered_map<Shader_r::id_t, std::tuple<std::string, std::string, int>> shaderDetails_;

    /* Models */
    std::unordered_map<Model_r::id_t, Model_r> models_;
    std::unordered_map<Model_r::id_t, int> modelCounts_;

    /* Textures */
    std::unordered_map<Texture_r::id_t, Texture_r> textures_;
    std::unordered_map<Texture_r::id_t, int> textureCounts_;

    ResourceManager();

public:

    /* Shaders */
    Shader getShader(const Shader_r::id_t& id);
    void increaseCount(Shader_r* shader);
    void decreaseCount(Shader_r* shader);

    /* Models */
    Model getModel(const Model_r::id_t& id);
    void increaseCount(Model_r* model);
    void decreaseCount(Model_r* model);

    /* Texutres */
    Texture getTexture(const Texture_r::id_t& id);
    void increaseCount(Texture_r* texture);
    void decreaseCount(Texture_r* texture);
};

}

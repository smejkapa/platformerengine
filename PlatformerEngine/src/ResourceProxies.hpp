#pragma once
#include "Shader_r.hpp"
#include "Model_r.hpp"
#include "Texture_r.hpp"

namespace pe {
    
template<typename TResource>
class ResourceProxy;

using Shader = ResourceProxy<Shader_r>;
using Model = ResourceProxy<Model_r>;
using Texture = ResourceProxy<Texture_r>;

}

// This breaks cyclic dependency of ResouceProxy and ResourceManager
// when including ResourceProxies
#include "ResourceProxy.hpp"

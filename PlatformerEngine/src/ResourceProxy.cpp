#include "ResourceProxy.hpp"

#include "ResourceManager.hpp"

namespace pe {

void increaseCount(Shader_r* shader) {
    ResourceManager::inst().increaseCount(shader);
}

void decreaseCount(Shader_r* shader) {
    ResourceManager::inst().decreaseCount(shader);
}

void increaseCount(Model_r* model) {
    ResourceManager::inst().increaseCount(model);
}

void decreaseCount(Model_r* model) {
    ResourceManager::inst().decreaseCount(model);
}

void increaseCount(Texture_r* texture) {
    ResourceManager::inst().increaseCount(texture);
}

void decreaseCount(Texture_r* texture) {
    ResourceManager::inst().decreaseCount(texture);
}
}

#pragma once

#include "ResourceManager.hpp"

namespace pe {

template <typename TResource>
class ResourceProxy {
protected:
    TResource* resource_;

public:
    ResourceProxy()
            : resource_(nullptr) {
    }

    explicit ResourceProxy(TResource* resource)
            : resource_(resource) {
        if (resource_ != nullptr)
            ResourceManager::inst().increaseCount(resource_);
    }

    virtual ~ResourceProxy() {
        if (resource_ != nullptr)
            ResourceManager::inst().decreaseCount(resource_);
    }

    ResourceProxy(const ResourceProxy& other)
            : resource_(other.resource_) {
        if (resource_ != nullptr)
            ResourceManager::inst().increaseCount(resource_);
    }

    ResourceProxy& operator=(const ResourceProxy& other) {
        if (this != &other) {
            resource_ = other.resource_;
            if (resource_ != nullptr)
                ResourceManager::inst().increaseCount(resource_);
        }

        return *this;
    }

    TResource* get() const { return resource_; }
};

}

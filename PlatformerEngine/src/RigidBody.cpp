#include "RigidBody.hpp"
#include "Physics.hpp"

namespace pe {

void RigidBody::enable() {
    Physics::inst().subscribe(this);
    rigidBody_->SetActive(true);
    colliderRenderer_.setIsActive(true);
}

void RigidBody::disable() {
    Physics::inst().unsubscribe(this);
    rigidBody_->SetActive(false);
    colliderRenderer_.setIsActive(false);
}

RigidBody::RigidBody(Entity* entity, b2Body* rigidBody)
        : entity_(entity)
        , rigidBody_(rigidBody)
        , colliderRenderer_(entity, rigidBody) {
    Physics::inst().subscribe(this);
}

RigidBody::~RigidBody() {
    Physics::inst().unsubscribe(this);
}

b2Body* RigidBody::getRigidBody() const {
    return rigidBody_;
}

void RigidBody::alignWithTransform() const {
    const auto entityPosition = entity_->getTransform().getGlobalPosition3();
    const auto entityRotation = entity_->getTransform().getGlobalRotation();
    rigidBody_->SetTransform({entityPosition.x, entityPosition.y}, entityRotation);
}

void RigidBody::updateTransform() const {
    const auto rigidBodyTransform = rigidBody_->GetTransform();
    entity_->getTransform().setPosition({rigidBodyTransform.p.x, rigidBodyTransform.p.y});
    entity_->getTransform().setRotation(rigidBodyTransform.q.GetAngle());
}
}

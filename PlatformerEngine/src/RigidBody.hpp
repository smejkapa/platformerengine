#pragma once
#include "Component.hpp"
#include "Entity.hpp"
#include "Box2D/Dynamics/b2Body.h"
#include "ColliderRenderer.hpp"

namespace pe {

class RigidBody : public Component {
    Entity* entity_;
    b2Body* rigidBody_;
    ColliderRenderer colliderRenderer_;

protected:
    void enable() override;
    void disable() override;

public:
    RigidBody(Entity* entity, b2Body* rigidBody);
    virtual ~RigidBody();

    // TODO implement operators
    RigidBody(const RigidBody&) = delete;
    RigidBody& operator=(const RigidBody&) = delete;
    RigidBody(RigidBody&&) = delete;
    RigidBody& operator=(RigidBody&&) = delete;

    b2Body* getRigidBody() const;

    void alignWithTransform() const;
    void updateTransform() const;
};

}

#include "Settings.hpp"

namespace pe {

constexpr int Settings::ScreenWidth = 1024;
constexpr int Settings::ScreenHeight = 768;
constexpr Vsync Settings::Vsync = Vsync::OFF;

}

#pragma once

namespace pe {

enum class Vsync {
    OFF = 0,
    ON = 1
};

struct Settings {
    static const int ScreenWidth;
    static const int ScreenHeight;
    static const Vsync Vsync;
    static constexpr auto ASSET_FOLDER = "assets/";
    static constexpr bool IsMultisamplingEnabled = true;
    static constexpr int MultisamplingLevel = 4;
};

}

#include "Shader_r.hpp"

#include <string>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>

namespace pe {

Shader_r::Shader_r(id_t id, const std::string& vertexPath, const std::string& fragmentPath, GLint instanceMVPLoc)
        : Resource<id_t>(id)
        , instanceMVPLoc_(instanceMVPLoc) {
    // Vertex shader
    std::string vertexCode = readFile(vertexPath);
    GLuint vertex = glCreateShader(GL_VERTEX_SHADER);
    compileShader(vertex, vertexCode);

    // Fragment shader
    std::string fragmentCode = readFile(fragmentPath);
    GLuint fragment = glCreateShader(GL_FRAGMENT_SHADER);
    compileShader(fragment, fragmentCode);

    // Linking
    shaderProgram_ = glCreateProgram();
    linkProgram(vertex, fragment);

    // Clean up
    glDeleteShader(vertex);
    glDeleteShader(fragment);

    MVPloc_ = glGetUniformLocation(shaderProgram_, "MVP");
}

Shader_r::~Shader_r() {
    if (!isMoved()) {
        glDeleteProgram(shaderProgram_);
    }
}

std::string Shader_r::readFile(const std::string& path) const {
    std::string code;
    std::ifstream file;
    file.exceptions(std::ifstream::failbit | std::ifstream::badbit);
    try {
        file.open(path);
        std::stringstream ss;
        ss << file.rdbuf();
        file.close();
        code = ss.str();
    } catch (const std::ifstream::failure&) {
        std::cout << "Error: Reading shader file." << std::endl;
    }

    return code;
}

void Shader_r::compileShader(GLint shaderId, const std::string& code) const {
    const GLchar* shaderCode = code.c_str();
    glShaderSource(shaderId, 1, &shaderCode, nullptr);
    glCompileShader(shaderId);

    // Check for errors
    GLint infoLogLength = 0;
    glGetShaderiv(shaderId, GL_INFO_LOG_LENGTH, &infoLogLength);
    if (infoLogLength > 1) // > 1 since \0 is included
    {
        std::vector<char> infoLog(infoLogLength);
        glGetShaderInfoLog(shaderId, infoLogLength, nullptr, infoLog.data());
        std::cout << infoLog.data() << std::endl;
    }
}

void Shader_r::linkProgram(GLint vertex, GLint fragment) const {
    glAttachShader(shaderProgram_, vertex);
    glAttachShader(shaderProgram_, fragment);
    glLinkProgram(shaderProgram_);

    // Check for errors
    GLint infoLogLength = 0;
    glGetProgramiv(shaderProgram_, GL_INFO_LOG_LENGTH, &infoLogLength);
    if (infoLogLength > 1) // > 1 since \0 is included
    {
        std::vector<char> infoLog(infoLogLength);
        glGetShaderInfoLog(shaderProgram_, infoLogLength, nullptr, infoLog.data());
        std::cout << infoLog.data() << std::endl;
    }
}

void Shader_r::activate() const {
    glUseProgram(shaderProgram_);
}

void Shader_r::deactivate() const {
    glUseProgram(0);
}

}

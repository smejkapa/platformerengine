#pragma once
#include "GL/glew.h" // Glew first I guess

#include <string>

#include "Resource.hpp"

namespace pe {

enum class ShaderName {
    None,
    UnlitColorInst,
    UnlitRedInst,
    SpriteTextured,
    SpriteTexturedInst,
    ColliderDebugDraw
};

class Shader_r : public Resource<ShaderName> {
    GLuint shaderProgram_;
    GLint instanceMVPLoc_;
    GLint MVPloc_;

    std::string readFile(const std::string& path) const;
    void compileShader(GLint shaderId, const std::string& code) const;
    void linkProgram(GLint vertex, GLint fragment) const;

public:
    static constexpr GLint LOCATION_NONE = -1;

    Shader_r(id_t id, const std::string& vertexPath, const std::string& fragmentPath, GLint instanceMVPLoc = LOCATION_NONE);
    ~Shader_r() override;

    // Disable copy since we have unmanaged resource (openGL program) and delete it in destructor
    Shader_r(const Shader_r&) = delete;
    Shader_r& operator=(const Shader_r&) = delete;

    Shader_r(Shader_r&& other) noexcept {
        *this = std::move(other);
    }

    Shader_r& operator=(Shader_r&& other) noexcept {
        if (this != &other) {
            shaderProgram_ = std::move(other.shaderProgram_);
            instanceMVPLoc_ = std::move(other.instanceMVPLoc_);
            MVPloc_ = std::move(other.MVPloc_);
            Resource<id_t>::operator=(std::move(other));
        }

        return *this;
    }

    void activate() const;
    void deactivate() const;
    GLuint getProgramId() const { return shaderProgram_; }
    GLint getInstanceMVPLoc() const { return instanceMVPLoc_; }
    GLint getMVPLoc() const { return MVPloc_; }
};

}

#include "SpriteRenderer.hpp"

namespace pe {
SpriteRenderer::SpriteRenderer(Entity* entity, const Material& material)
        : Renderer(entity, material, "quad.obj") {
}
}

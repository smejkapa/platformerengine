#pragma once

#include "Renderer.hpp"

namespace pe {

class SpriteRenderer : public Renderer {
public:
    SpriteRenderer(Entity* entity, const Material& material);
};

}

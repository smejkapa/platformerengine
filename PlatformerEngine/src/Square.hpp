#pragma once
#include "Color.hpp"
#include "glm/glm.hpp"

namespace pe {

struct Square {
    Square(const glm::vec2& topLeft, const glm::vec2& botRight)
            : Square(topLeft, botRight, pe::Color()) {
    }

    Square(const glm::vec2& topLeft, const glm::vec2& botRight, const Color& color)
            : TopLeft(topLeft)
            , BotRight(botRight)
            , Color(color) {
    }

    glm::vec2 TopLeft;
    glm::vec2 BotRight;
    Color Color;
};

}

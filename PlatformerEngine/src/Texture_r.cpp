#include "Texture_r.hpp"
#include <SOIL/SOIL.h>
#include "Settings.hpp"

namespace pe {

GLuint Texture_r::loadFromFile(const std::string& file) const {
    // Create and load
    GLuint textureID;
    glGenTextures(1, &textureID);

    int width, height;
    auto image = SOIL_load_image(std::string(Settings::ASSET_FOLDER + file).c_str(), &width, &height, nullptr, SOIL_LOAD_RGBA);
    glBindTexture(GL_TEXTURE_2D, textureID);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);

    glGenerateMipmap(GL_TEXTURE_2D);

    // Setup texture
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    // Cleanup
    glBindTexture(GL_TEXTURE_2D, 0);
    SOIL_free_image_data(image);

    return textureID;
}

Texture_r::Texture_r(const id_t& file)
        : Resource<id_t>(file)
        , textureId_(loadFromFile(file)) {
}

Texture_r::~Texture_r() {
    if (!isMoved()) {
        glDeleteTextures(1, &textureId_);
    }
}

void Texture_r::activate() const {
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textureId_);
}

void Texture_r::deactivate() const {
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, 0);
}

}

#pragma once

#include "GL/glew.h" // Glew first

#include "Resource.hpp"
#include <string>
#include "gl/GL.h"

namespace pe {

class Texture_r
        : public Resource<std::string> {
private:
    GLuint textureId_;

    GLuint loadFromFile(const std::string& file) const;

public:
    explicit Texture_r(const id_t& file);
    ~Texture_r() override;

    Texture_r(const Texture_r&) = delete;
    Texture_r& operator=(const Texture_r&) = delete;

    Texture_r(Texture_r&& other) noexcept {
        *this = std::move(other);
    }

    Texture_r& operator=(Texture_r&& other) noexcept {
        if (this != &other) {
            textureId_ = std::move(other.textureId_);
            Resource<id_t>::operator=(std::move(other));
        }

        return *this;
    }

    void activate() const;
    void deactivate() const;
};

}

#include "Update.hpp"

namespace pe {

void Update::subscribe(IUpdatable* updatable) {
    updatables_.insert(updatable);
}

void Update::unsubscribe(IUpdatable* updatable) {
    updatables_.erase(updatable);
}

void Update::update() {
    for(const auto& updatable : updatables_) {
        updatable->update();
    }
}
}

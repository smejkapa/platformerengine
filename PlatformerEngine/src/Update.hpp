#pragma once
#include "System.hpp"
#include <unordered_set>
#include "Singleton.hpp"
#include "IUpdatable.hpp"

namespace pe {
class Update 
        : public Singleton<Update>
        , public System {
    friend Singleton<Update>;

private:
    std::unordered_set<IUpdatable*> updatables_;

public:
    void subscribe(IUpdatable* updatable);
    void unsubscribe(IUpdatable* updatable);

    void update();
};
}

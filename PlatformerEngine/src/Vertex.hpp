#pragma once
#include <glm/glm.hpp>

namespace pe {

struct PointVertex {
    glm::vec3 Position;
};

struct Vertex {
    glm::vec3 Position;
    glm::vec3 Normal;
    glm::vec2 TexCoord;
};

}

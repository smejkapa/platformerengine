#include "Transform.hpp"

namespace pe {

void Transform::calculateTranslation() {
    translation_ = glm::translate(glm::mat4(1.0f), position_);
}

void Transform::calculateScale() {
    scaleMatrix_ = glm::scale(glm::mat4(1.0f), scale_);
}

void Transform::calculateRotation() {
    rotationMatrix_ = glm::rotate(glm::mat4(1.0f), rotation_, glm::vec3(0, 0, 1));
}

void Transform::calculateModel() {
    calculateTranslation();
    calculateScale();
    calculateRotation();
    calculateOnlyModel();
}

void Transform::calculateOnlyModel() {
    modelMatrix_ = translation_ * rotationMatrix_ * scaleMatrix_;
}

void Transform::addChild(Transform* child) {
    children_.insert(child);
}

void Transform::removeChild(Transform* child) {
    children_.erase(child);
}

Transform::Transform(const glm::vec3& position, const float rotation, const glm::vec3& scale)
        : position_(position)
        , scale_(scale)
        , rotation_(rotation) {
    calculateModel();
}

glm::vec3 Transform::getPosition3() const {
    return position_;
}

glm::vec2 Transform::getPosition() const {
    return glm::vec2{position_.x, position_.y};
}

glm::vec3 Transform::getGlobalPosition3() const {
    if (parent_ == nullptr) {
        return getPosition3();
    }

    const auto parentModelMatrix = parent_->getModelMatrix();
    const glm::vec4 pos = parentModelMatrix * glm::vec4(position_.x, position_.y, position_.z, 1.0f);
    return glm::vec3(pos.x, pos.y, pos.z);
}

glm::vec2 Transform::getGlobalPosition() const {
    const auto globalPos3 = getGlobalPosition3();
    return glm::vec2(globalPos3.x, globalPos3.y);
}

glm::vec3 Transform::getScale() const {
    return scale_;
}

float Transform::getRotation() const {
    return rotation_;
}

float Transform::getGlobalRotation() const {
    if (parent_ == nullptr) {
        return rotation_;
    }
    return rotation_ + parent_->getGlobalRotation();
}

Entity* Transform::getEntity() const {
    return entity_;
}

void Transform::setEntity(Entity* entity) {
    entity_ = entity;
}

const Transform* Transform::getParent() const {
    return parent_;
}

void Transform::setPosition(const glm::vec3& newPosition) {
    position_ = newPosition;
    calculateTranslation();
    calculateOnlyModel();
}

void Transform::setPosition(const glm::vec2& newPosition) {
    setPosition({newPosition.x, newPosition.y, position_.z});
}

void Transform::setScale(const glm::vec3& newScale) {
    scale_ = newScale;
    calculateScale();
    calculateOnlyModel();
}

void Transform::setRotation(const float rotation) {
    rotation_ = rotation;
    calculateRotation();
    calculateOnlyModel();
}

void Transform::setParent(Transform* parent) {
    if (parent_ != nullptr) {
        parent_->removeChild(this);
    }
    parent_ = parent;
    parent_->addChild(this);
}

const std::unordered_set<Transform*>& Transform::getChildren() const {
    return children_;
}

glm::mat4 Transform::getModelMatrix() const {
    if (parent_ != nullptr) {
        return parent_->getModelMatrix() * modelMatrix_;
    } else {
        return modelMatrix_;
    }
}
}

#pragma once

#include "glm/glm.hpp"
#include <glm/gtc/matrix_transform.hpp>
#include <unordered_set>

namespace pe {

class Entity;

class Transform {
private:
    Entity* entity_;
    Transform* parent_ = nullptr;
    std::unordered_set<Transform*> children_;
    
    // Transform matrices
    glm::mat4 translation_{};
    glm::mat4 scaleMatrix_{};
    glm::mat4 rotationMatrix_{};
    glm::mat4 modelMatrix_{};

    glm::vec3 position_;
    glm::vec3 scale_;

    float rotation_;

    void calculateTranslation();
    void calculateScale();
    void calculateRotation();
    void calculateModel();
    void calculateOnlyModel();

    void addChild(Transform* child);
    void removeChild(Transform* child);

public:
    explicit Transform(
        const glm::vec3& position = glm::vec3(),
        float rotation = 0.0f,
        const glm::vec3& scale = glm::vec3(1, 1, 1)
    );

    glm::vec3 getPosition3() const;
    glm::vec2 getPosition() const;

    glm::vec3 getGlobalPosition3() const;
    glm::vec2 getGlobalPosition() const;

    glm::vec3 getScale() const;
    float getRotation() const;
    float getGlobalRotation() const;

    Entity* getEntity() const;
    void setEntity(Entity* entity);

    const Transform* getParent() const;
    void setParent(Transform* parent);

    const std::unordered_set<Transform*>& getChildren() const;

    void setPosition(const glm::vec3& newPosition);
    void setPosition(const glm::vec2& newPosition);
    void setScale(const glm::vec3& newScale);
    void setRotation(float rotation);

    glm::mat4 getModelMatrix() const;
};

}

#include "Character.hpp"

#include "../util/Input.hpp"
#include "../CameraManager.hpp"
#include "../MessagePayloadImpl.hpp"
#include "Box2D/Dynamics/b2Body.h"
#include "Physics.hpp"


namespace tg {

void Character::initRigidBody() {
    b2BodyDef bodyDef;
    bodyDef.position.SetZero();
    bodyDef.type = b2_dynamicBody;
    bodyDef.fixedRotation = true;
    bodyDef.linearDamping = 0.0f;

    const auto rigidBody = pe::Physics::inst().getWorld().CreateBody(&bodyDef);

    b2PolygonShape box{};
    box.SetAsBox(0.5f, 1.0f, b2Vec2(0.0f, 0.0f), 0.0f);

    b2FixtureDef fixtureDef{};
    fixtureDef.shape = &box;
    fixtureDef.density = 1000.0f;
    fixtureDef.friction = 0.0f;

    rigidBody->CreateFixture(&fixtureDef);

    rigidBody_ = addComponent<pe::RigidBody>(this, rigidBody);
}

Character::Character()
        : speed_(10.0f, 10.0f, 0.0f) {
    spriteRenderer_ = addComponent<pe::SpriteRenderer>(
        this, 
        pe::Material(
            pe::ResourceManager::inst().getShader(pe::ShaderName::SpriteTextured), 
            pe::ResourceManager::inst().getTexture("pandak.png")
        )
    );

    // Place character in front of tiles
    Entity::moveTo(glm::vec3(0, 0, 1));
    transform_.setScale(glm::vec3(1, 2, 1));

    // Bind keys to movement actions
    pe::Message m(pe::Message::Type::Action);
    m.payload = pe::ActionPl::create(pe::ActionPl::Type::MoveLeft);
    pe::util::Input::inst().bindKey(pe::util::KeyCode::A, pe::util::KeyState::Down, std::move(m));

    pe::Message m2(pe::Message::Type::Action);
    m2.payload = pe::ActionPl::create(pe::ActionPl::Type::MoveRight);
    pe::util::Input::inst().bindKey(pe::util::KeyCode::D, pe::util::KeyState::Down, std::move(m2));

    pe::Message m3(pe::Message::Type::Action);
    m3.payload = pe::ActionPl::create(pe::ActionPl::Type::MoveUp);
    pe::util::Input::inst().bindKey(pe::util::KeyCode::W, pe::util::KeyState::Down, std::move(m3));

    pe::Message m4(pe::Message::Type::Action);
    m4.payload = pe::ActionPl::create(pe::ActionPl::Type::MoveDown);
    pe::util::Input::inst().bindKey(pe::util::KeyCode::S, pe::util::KeyState::Down, std::move(m4));

    initRigidBody();
}

void Character::update() {
    auto velocity = rigidBody_->getRigidBody()->GetLinearVelocity();

    // X movement is instant
    velocity.x = movement_.x;

    // Y movement respects gravity
    if (movement_.y != 0) {
        velocity.y = movement_.y;
    }

    rigidBody_->getRigidBody()->SetLinearVelocity(velocity);

    movement_ = glm::vec3();
}

void Character::handleMessage(const pe::Message& msg) {
    switch (msg.type) {
        case pe::Message::Type::Action: {
            const auto pl = dynamic_cast<pe::ActionPl*>(msg.payload);
            assert(pl);
            switch (pl->Action) {
                case pe::ActionPl::Type::MoveLeft:
                    movement_ = movement_ + glm::vec3(-speed_.x, 0.0f, 0.0f);
                    break;
                case pe::ActionPl::Type::MoveRight:
                    movement_ = movement_ + glm::vec3(speed_.x, 0.0f, 0.0f);
                    break;
                case pe::ActionPl::Type::MoveUp:
                    movement_ = movement_ + glm::vec3(0.0f, speed_.y, 0.0f);
                    break;
                case pe::ActionPl::Type::MoveDown:
                    movement_ = movement_ + glm::vec3(0.0f, -speed_.y, 0.0f);
                    break;
                default:
                    break;
            }
        }
        default:
            break;
    }
}

}

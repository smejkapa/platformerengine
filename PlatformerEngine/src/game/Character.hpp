#pragma once

#include "GL/glew.h" // Glew first I guess

#include "../Entity.hpp"
#include "../IMessageHandler.hpp"
#include "../SpriteRenderer.hpp"
#include "RigidBody.hpp"

namespace tg {

class Character
        : public pe::Entity
        , public pe::IMessageHandler {
private:
    pe::SpriteRenderer* spriteRenderer_;
    pe::RigidBody* rigidBody_;

    const glm::vec3 speed_;
    glm::vec3 movement_;

    void initRigidBody();

public:
    Character();

    void update() override;
    void handleMessage(const pe::Message&) override;
};

}

#include "TerrariaGame.hpp"
#include "../CameraManager.hpp"
#include "util/Time.hpp"
#include "util/Input.hpp"


namespace tg {

using namespace pe::util;

void TerrariaGame::init() {
    pe::CameraManager::inst().setMainCamera(&camera_);

    // Init background rendering
    //tiles_ = generateChessboard();
    auto t = generateTerrain();
    tiles_.insert(tiles_.end(), std::make_move_iterator(t.begin()), std::make_move_iterator(t.end()));
    parent_.getTransform().setPosition(glm::vec2(2, 2));
    parentTile_ = std::make_unique<Tile>(pe::Transform(glm::vec3(0, 0, 0.0f)), Tile::Type::Dirt);
    child_ = std::make_unique<Tile>(pe::Transform(glm::vec3(2, 2, 0.0f)), Tile::Type::Dirt);

    child_->getTransform().setParent(&parent_.getTransform());
    parentTile_->getTransform().setParent(&parent_.getTransform());

    character_.moveTo(glm::vec3(-8, -4, 1));

    pe::Physics::inst().setGravity({0, -40.0f});
}

std::vector<std::unique_ptr<Tile>> TerrariaGame::generateChessboard() const {
    constexpr int startX = -100;
    constexpr int startY = -100;
    constexpr int endX = 100;
    constexpr int endY = -11;

    std::vector<std::unique_ptr<Tile>> tiles;

    for (int x = startX; x < endX; x++) {
        for (int y = startY; y < endY; y++) {
            //if ((x + y) & 1)
            tiles.push_back(std::make_unique<Tile>(pe::Transform(glm::vec3(x, y, 0.0f)), Tile::Type::Dirt));
        }
    }

    return tiles;
}

std::vector<std::unique_ptr<Tile>> TerrariaGame::generateTerrain() const {
    constexpr int startX = -15;
    constexpr int startY = -10;
    constexpr int endX = 15;
    constexpr int endY = 10;

    std::vector<std::unique_ptr<Tile>> tiles;

    for (int x = startX; x < endX; x++) {
        for (int y = startY; y < endY; y++) {
            if (x == startX || x == endX - 1 || y == startY || y == endY - 1) {
                auto type = Tile::Type::None;
                if (y == startY)
                    type = Tile::Type::Dirt;
                tiles.push_back(std::make_unique<Tile>(pe::Transform(glm::vec3(x, y, 0.0f)), type));
            }
        }
    }

    // House
    for (int x = 0; x < 5; ++x) {
        for (int y = 0; y < 3; ++y) {
            if (x == 0 || x == 4 || y == 2)
                tiles.push_back(std::make_unique<Tile>(pe::Transform(glm::vec3(x, y - 9, 0.0f)), Tile::Type::Wood));
        }
    }

    return tiles;
}

float rotation = 0.0f;

void TerrariaGame::update() {
    if (Input::inst().getKeyDown(E)) {
        character_.setIsActive(!character_.getIsActive());
    }

    auto camPos = character_.getTransform().getPosition3();
    camPos.z = 10.f;
    camera_.moveTo(camPos);

    rotation += glm::radians(45.0f) * Time::inst().delta();
    //rotation = glm::radians(45.f);
    parent_.getTransform().setRotation(rotation);
    child_->getTransform().setRotation(rotation);
}

void TerrariaGame::handleMessage(const pe::Message&) {
}

}

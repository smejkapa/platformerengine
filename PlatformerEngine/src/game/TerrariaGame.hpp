#pragma once
#include "GL/glew.h" // Glew first I guess

#include "../IGame.hpp"
#include "Character.hpp"
#include <vector>
#include "Tile.hpp"
#include "../OrthoCamera.hpp"


namespace tg {

class TerrariaGame
        : public pe::IGame {
private:
    pe::OrthoCamera camera_;

    Character character_;

    std::vector<std::unique_ptr<Tile>> tiles_;
    pe::Entity parent_;
	std::unique_ptr<Tile> parentTile_;
	std::unique_ptr<Tile> child_;

    std::vector<std::unique_ptr<Tile>> generateChessboard() const;
    std::vector<std::unique_ptr<Tile>> generateTerrain() const;

public:
    void init() override;
    void update() override;
    void handleMessage(const pe::Message&) override;
};

}

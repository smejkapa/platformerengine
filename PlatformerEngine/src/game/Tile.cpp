#include "Tile.hpp"

namespace tg {

Tile::Tile(const Type type) : Tile(pe::Transform(), type) {
}

Tile::Tile(const pe::Transform transform, const Type type)
        : Entity(transform) {
    spriteRenderer_ = addComponent<pe::SpriteRenderer>(
        this, 
        pe::Material(
            getShaderForTile(type), 
            getTextureForTile(type)
        )
    );
    
    b2BodyDef bodyDef;
    bodyDef.position.Set(transform_.getPosition3().x, transform.getPosition3().y);

    const auto rigidBody = pe::Physics::inst().getWorld().CreateBody(&bodyDef);

    b2PolygonShape box;
    //box.SetAsBox(0.5f, 0.5f, {0.5f, 0.5f}, 0);
    box.SetAsBox(0.5f, 0.5f);

    b2FixtureDef fixtureDef{};
    fixtureDef.friction = 1.0f;
    fixtureDef.shape = &box;

    rigidBody->CreateFixture(&fixtureDef);

    rigidBody_ = addComponent<pe::RigidBody>(this, rigidBody);
}

void Tile::update() {
}

pe::Texture Tile::getTextureForTile(const Type type) {
    std::string name;
    switch (type) {
        case Type::None:
            return pe::Texture();
        case Type::Dirt:
            name = "dirt.png";
            break;
        case Type::Wood:
            name = "wood.png";
            break;
        default:
            throw std::exception("Type not handled"); // TODO use better exception
    }

    return pe::ResourceManager::inst().getTexture(name);
}

pe::Shader Tile::getShaderForTile(const Type type) {
    return pe::ResourceManager::inst().getShader(
        type == Type::None
        ? pe::ShaderName::UnlitColorInst
        : pe::ShaderName::SpriteTexturedInst
    );
}

}

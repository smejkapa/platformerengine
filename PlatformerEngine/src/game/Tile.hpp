#pragma once

#include "../SpriteRenderer.hpp"
#include "../Physics.hpp"

namespace tg {

class Tile : public pe::Entity {
private:
    pe::SpriteRenderer* spriteRenderer_;
    pe::RigidBody* rigidBody_;

public:
    enum class Type {
        None,
        Dirt,
        Wood
    };

    explicit Tile(const Type type = Type::None);
    explicit Tile(const pe::Transform transform, const Type type = Type::None);

    void update() override;

private:
    static pe::Texture getTextureForTile(const Type type);
    static pe::Shader getShaderForTile(const Type type);
};

}

#include "FpsDisplay.hpp"

#include "Time.hpp"

//#include "GLFW/glfw3.h"
#include <cstdio>
#include <iostream>
#include <sstream>

namespace pe {
namespace util {

void FpsDisplay::showFps(GLFWwindow* window) {
    if (isOn_) {
        elapsedTotal_ += Time::inst().delta();

        ++frameCount_;
        if (elapsedTotal_ >= interval_) {
            const auto frameTime = elapsedTotal_ / frameCount_ * 1000;
            const auto fps = frameCount_ / elapsedTotal_;
            std::stringstream fpsString;
            fpsString << "Platformer engine | " << "Frame time: " << frameTime << " ms" << "   (" << fps << " fps)";
            glfwSetWindowTitle(window, fpsString.str().c_str());

            //std::cout << "Frame time: " << elapsedTotal_ / frameCount_ * 1000 << " ms" << "   (" << frameCount_ / elapsedTotal_ << " fps)" << std::endl;
            frameCount_ = 0;
            elapsedTotal_ = 0.0;
        }
    }
}

void FpsDisplay::enable() {
    printf("FpsDisplay enabled\n");
    isOn_ = true;
    frameCount_ = 0;
    elapsedTotal_ = 0.0;
}

void FpsDisplay::disable() {
    printf("FpsDisplay disabled\n");
    isOn_ = false;

}

void FpsDisplay::toggle() {
    if (isOn_) {
        disable();
    } else {
        enable();
    }
}

}
}

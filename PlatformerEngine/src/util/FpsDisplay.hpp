#pragma once
#include <GLFW/glfw3.h>

namespace pe {
namespace util {

class FpsDisplay {
    bool isOn_;
    double elapsedTotal_;
    double interval_;
    int frameCount_;

public:
    FpsDisplay()
            : isOn_(true)
            , elapsedTotal_(0.0)
            , interval_(1.0)
            , frameCount_(0) {
    }

    void showFps(GLFWwindow* window);
    void enable();
    void disable();
    void toggle();
};

}
}

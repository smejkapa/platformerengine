#include "GL/glew.h" // Glew first I guess

#include "Input.hpp"

#include <algorithm>
#include "../MessageBus.hpp"
#include "../MessagePayloadImpl.hpp"

namespace pe {

namespace util {
    // ==================
    // Input::KeyBinding
    // ==================
    bool Input::KeyBinding::isActive() const {
        switch (action_) {
            case KeyState::Down:
                return inst().getKey(key_);
            case KeyState::Pressed:
                return inst().getKeyDown(key_);
            case KeyState::Released:
                return inst().getKeyUp(key_);
            default: 
                return false;
        }
    }

    // =====
    // Input
    // =====
    bool Input::getKeyDown(const KeyCode& key) const {
        return !lastKeyMap_[key] && keyMap_[key];
    }

    bool Input::getKeyUp(const KeyCode& key) const {
        return lastKeyMap_[key] && !keyMap_[key];
    }

    bool Input::getKey(const KeyCode& key) const {
        return keyMap_[key];
    }

    void Input::update() {
        updateKeyMaps();

        for (const auto& b : bindings_) {
            if (b.isActive()) {
                MessageBus::inst().sendMessage(b.msg_);
            }
        }
    }

    void Input::updateKeyMaps() {
        // std::copy produces a warning because of unchecked iterators  vvv
        //std::copy(std::begin(KeyMap), std::end(KeyMap), std::begin(LastKeyMap));
        memcpy(lastKeyMap_.data(), keyMap_.data(), sizeof(bool) * KEY_MAP_SIZE);

        for (auto&& k : keyMap_) {
            k = false;
        }

        if (glfwGetKey(window_, GLFW_KEY_A) == GLFW_PRESS)
            keyMap_[KeyCode::A] = true;
        if (glfwGetKey(window_, GLFW_KEY_B) == GLFW_PRESS)
            keyMap_[KeyCode::B] = true;
        if (glfwGetKey(window_, GLFW_KEY_C) == GLFW_PRESS)
            keyMap_[KeyCode::C] = true;
        if (glfwGetKey(window_, GLFW_KEY_D) == GLFW_PRESS)
            keyMap_[KeyCode::D] = true;
        if (glfwGetKey(window_, GLFW_KEY_E) == GLFW_PRESS)
            keyMap_[KeyCode::E] = true;
        if (glfwGetKey(window_, GLFW_KEY_F) == GLFW_PRESS)
            keyMap_[KeyCode::F] = true;
        if (glfwGetKey(window_, GLFW_KEY_G) == GLFW_PRESS)
            keyMap_[KeyCode::G] = true;
        if (glfwGetKey(window_, GLFW_KEY_H) == GLFW_PRESS)
            keyMap_[KeyCode::H] = true;
        if (glfwGetKey(window_, GLFW_KEY_I) == GLFW_PRESS)
            keyMap_[KeyCode::I] = true;
        if (glfwGetKey(window_, GLFW_KEY_J) == GLFW_PRESS)
            keyMap_[KeyCode::J] = true;
        if (glfwGetKey(window_, GLFW_KEY_K) == GLFW_PRESS)
            keyMap_[KeyCode::K] = true;
        if (glfwGetKey(window_, GLFW_KEY_L) == GLFW_PRESS)
            keyMap_[KeyCode::L] = true;
        if (glfwGetKey(window_, GLFW_KEY_M) == GLFW_PRESS)
            keyMap_[KeyCode::M] = true;
        if (glfwGetKey(window_, GLFW_KEY_N) == GLFW_PRESS)
            keyMap_[KeyCode::N] = true;
        if (glfwGetKey(window_, GLFW_KEY_O) == GLFW_PRESS)
            keyMap_[KeyCode::O] = true;
        if (glfwGetKey(window_, GLFW_KEY_P) == GLFW_PRESS)
            keyMap_[KeyCode::P] = true;
        if (glfwGetKey(window_, GLFW_KEY_Q) == GLFW_PRESS)
            keyMap_[KeyCode::Q] = true;
        if (glfwGetKey(window_, GLFW_KEY_R) == GLFW_PRESS)
            keyMap_[KeyCode::R] = true;
        if (glfwGetKey(window_, GLFW_KEY_S) == GLFW_PRESS)
            keyMap_[KeyCode::S] = true;
        if (glfwGetKey(window_, GLFW_KEY_T) == GLFW_PRESS)
            keyMap_[KeyCode::T] = true;
        if (glfwGetKey(window_, GLFW_KEY_U) == GLFW_PRESS)
            keyMap_[KeyCode::U] = true;
        if (glfwGetKey(window_, GLFW_KEY_V) == GLFW_PRESS)
            keyMap_[KeyCode::V] = true;
        if (glfwGetKey(window_, GLFW_KEY_W) == GLFW_PRESS)
            keyMap_[KeyCode::W] = true;
        if (glfwGetKey(window_, GLFW_KEY_X) == GLFW_PRESS)
            keyMap_[KeyCode::X] = true;
        if (glfwGetKey(window_, GLFW_KEY_Y) == GLFW_PRESS)
            keyMap_[KeyCode::Y] = true;
        if (glfwGetKey(window_, GLFW_KEY_Z) == GLFW_PRESS)
            keyMap_[KeyCode::Z] = true;
    }


    void Input::shutDown() {
        bindings_.clear();
    }

    void Input::setWindow(GLFWwindow* window) {
        window_ = window;
        glfwSetKeyCallback(window_, keyCallback);
    }

    void Input::keyCallback(GLFWwindow*, int key, int scancode, int action, int mode) {
        inst().keyCallback(key, scancode, action, mode);
    }

    void Input::keyCallback(int key, int scancode, int action, int mode) const {
        Message::Type t;
        switch (action) {
            case GLFW_PRESS:
            case GLFW_REPEAT: t = Message::Type::ButtonDown;
                break;
            case GLFW_RELEASE: t = Message::Type::ButtonReleased;
                break;

            default: t = Message::Type::None;
                break;
        }

        Message m(t);
        m.payload = InputPl::create(key, scancode, mode, action == GLFW_PRESS);
        MessageBus::inst().sendMessage(m);
    }

    void Input::bindKey(KeyCode key, KeyState action, Message&& msg) {
        KeyBinding kb(key, action, std::move(msg));
        bindings_.push_back(std::move(kb));
    }
}

}

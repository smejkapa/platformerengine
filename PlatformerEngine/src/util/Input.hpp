#pragma once

#include "GLFW/glfw3.h"
#include <vector>
#include <array>

#include "../Message.hpp"
#include "../Singleton.hpp"
#include "KeyCode.hpp"

namespace pe {
namespace util {

enum class KeyState {
    Pressed,
    Released,
    Down
};

class Input
        : public Singleton<Input> {
    friend Singleton<Input>;

    struct KeyBinding {
        KeyCode key_;
        KeyState action_;
        Message msg_;

        KeyBinding(KeyCode key, KeyState action, Message&& msg)
                : key_(key)
                , action_(action)
                , msg_(std::move(msg)) {
        }

        bool isActive() const;
    };

    static constexpr size_t KEY_MAP_SIZE = KeyCode::KeyCount;

    std::array<bool, KEY_MAP_SIZE> lastKeyMap_;
    std::array<bool, KEY_MAP_SIZE> keyMap_;

    GLFWwindow* window_;

    std::vector<KeyBinding> bindings_;

    Input() :
        window_(nullptr) {
    }

    void updateKeyMaps();

public:
    void shutDown();

    void setWindow(GLFWwindow* window);

    void update();

    bool getKeyDown(const KeyCode& key) const;
    bool getKeyUp(const KeyCode& key) const;
    bool getKey(const KeyCode& key) const;

    void keyCallback(int key, int scancode, int action, int mode) const;
    static void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mode);

    void bindKey(KeyCode, KeyState, Message&&);
};

}
}

#include "Time.hpp"

namespace pe {
namespace util {

void Time::update() {
    const double currentTime = glfwGetTime();
    delta_ = currentTime - lastUpdate_;
    lastUpdate_ = currentTime;
}

}
}

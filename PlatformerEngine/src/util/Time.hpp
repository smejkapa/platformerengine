#pragma once

#include "GLFW/glfw3.h"

#include "../IUpdatable.hpp"
#include "../Singleton.hpp"

namespace pe {
namespace util {

class Time
        : public Singleton<Time>,
        public IUpdatable {
    friend Singleton<Time>;

    Time()
            : lastUpdate_(glfwGetTime())
            , delta_(0.0) {
    }

    double lastUpdate_;
    double delta_;

public:
    void update() override;

    float delta() const {
        return static_cast<float>(delta_);
    }
};

}
}

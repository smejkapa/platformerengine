# Platformer engine

Platformer engine is a learning project where I explore how to tackle problems of much bigger 3D game engines such as resource management, system communication, and decoupling of rendering backend and the game. It is written in modern C++ and OpenGL using [GLM](https://glm.g-truc.net/) for math and [Box2D](box2d.org) for physics.

## Getting started

The things you need

* Visual studio 2017 (community should suffice)
* [Git LFS](https://git-lfs.github.com/)

All the libraries are included in the project.

To run the project from Visual Studio you need to copy (or [symlink](http://schinagl.priv.at/nt/hardlinkshellext/linkshellextension.html)) `PlatformerEngine/assets` and `PlatformerEngine/shaders` to the build directories - `x64/Debug` and `x64/Release`.

## Features

### Entity component system

In our case this means that we have different systems such as `Draw`, `Input`, or `ResourceManager` and these provide us with components from which we assemble the objects in the game. These components are then updated and communicate with their appropriate systems. This way it is very easy to create new game objects and reuse different components without the need to create an inheritance hierarchy. It is also very easy to work with the components from the system perspective as we do in case of the instanced rendering.

### Automated instanced rendering

Instanced rendering can drastically improve performance, especially when rendering many identical objects. In our case, when we consider tile-based 2D platformers, this can be hugely beneficial. It would be, however, very inconvenient for the game programmer to handle all the instancing by hand. That's why we introduce a system which automatically groups objects with the same geometry and material together, and these are then rendered in one draw call.

This process is automated in a way, that when we create a `Renderer` subscribes to the `Draw` system and is grouped with others. When the component is deleted, it is automatically unsubscribed from the drawing.

This could be exploited even more if we used one texture atlas for all the textures used by tiles.

### Automated resource management

`ResourceManager` takes care of loading and unloading resources as they are needed. When a game objects wants a resource, e.g., a texture for its rendering component, it simply asks the `ResourceManager`. If the resource is not loaded yet, or was unloaded already, it is loaded and registered in the `ResourceManager`. To the caller, a proxy class is returned. This proxy class implements reference counting so the `ResourceManager` can easily see which resources are not used anymore.

Similar system would work for open world games, where not the whole world can fit into the memory and parts of it need to be unloaded and loaded only when needed.

To implement resources and proxy classes we use C++ move semantics, i.e., resources are move only and only one instance of given resource exists at a time.

### Inter-system communication using message bus

To avoid design where every system has a handle to every other system and calls it directly, we use message bus pattern, where there is a `MessageBus` singleton system which accepts messages and passes them to anyone subscribed. Inspired by this [gamasutra article](https://www.gamasutra.com/blogs/MichaelKissner/20151027/257369/Writing_a_Game_Engine_from_Scratch__Part_1_Messaging.php).

This system works well, however, many messages may go to subscribers who do not care about them which is a tradeoff for its generality. Handling messages using events may be better approach, e.g., each object is subscribed only for events it cares about.

### Physic system

Physics system uses Box2D as the physics backend and provides components such as RigidBody which can be attached to entities to enable physical interaction. For debug purposes the colliders have ability to draw by adding `ColliderRenderer` component which is automatically drawn by the Draw system.

## Other features

* Transform hierarchy - transforms have parents/children and operate in different spaces
* Update system - only active entities are updated
* Basic input manager with key bindings

TODO:

* Improve Box2D integration
* BSP to render only what is visible (probably BVH or hashed grid)
* Triggers
* Implement a game

## More

More notes can be found in [this Google doc](https://docs.google.com/document/d/13ZgNjJ4pzVSkll_5Zuopy3TJz6Vbh7Ezdc1i9FXTpvs/edit?usp=sharing).
